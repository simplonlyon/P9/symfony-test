<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Dog;

class DogFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        
        for($x = 1; $x <= 5; $x++) {
            $dog = new Dog();
            $dog->setName('name'.$x);
            $dog->setBreed('breed'.$x);
            $dog->setAge($x);
            $manager->persist($dog);
        }

        $manager->flush();
    }
}
