<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;



class RandomControllerTest extends WebTestCase
{


    public function testShowNumber()
    {
        $this->assertEquals(10, 10);

        $client = static::createClient();

        $crawler = $client->request('GET', '/random');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $para = $crawler->filter('p');

        // if ($para->text() > 7) {

        //     $this->assertContains('Winner', $crawler->text());
        // }

        $this->assertGreaterThan(0, $para->text());
        $this->assertLessThan(11, $para->text());
    }
}

